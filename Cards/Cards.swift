//
//  Cards.swift
//  Cards
//
//  Created by Edward Arenberg on 9/23/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

import Foundation

struct Card : Printable {
    enum Rank : Int,Printable {
        case Ace=1,Two,Three,Four,Five,Six,Seven,Eight,Nine,Ten,Jack,Queen,King
        var description: String {
            switch self.toRaw() {
            case 1:
                return "A"
            case 2...9:
                return "\(self.toRaw())"
            case 10:
                return "T"
            case 11:
                return "J"
            case 12:
                return "Q"
            case 13:
                return "K"
            default:
                return "U"
            }
        }

    }
    enum Suit : String,Printable {
        case Spade="♤",Heart="♡",Diamond="♢",Club="♧"
        var description: String {
            return self.toRaw()
        }
    }

    // Card value - rank and suit
    var rank : Rank = .Ace
    var suit : Suit = .Spade
    
    var description: String {
        return "\(rank)\(suit)"
    }
    
    /* Alternate way to hold a card value (rank,suit) using a tuple
    var card : (rank:Rank,suit:Suit) = (.Ace,.Spade)
    
    var description: String {
        return "\(card.rank)\(card.suit)"
    }
    */
}

class Deck {
    var cards : [Card] = []
    
    init() {
        for s : Card.Suit in [.Spade,.Heart,.Diamond,.Club] {
            for i in 1...13 {
                let card = Card(rank: Card.Rank.fromRaw(i)!, suit: s)
//                let card = Card(card: (Card.Rank.fromRaw(i)!,s))  // Alternate card representation
                cards.append(card)
            }
        }
    }

    // Simple shuffle routine swaps every card position
    func shuffle() {
        let numCards : Int = cards.count
        for i in 0..<numCards {
            let j = Int(arc4random()) % numCards
            let swapCard = cards[i]
            cards[i] = cards[j]
            cards[j] = swapCard
        }
    }
}
