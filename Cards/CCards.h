//
//  CCards.h
//  Cards
//
//  Created by Edward Arenberg on 9/23/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

#ifndef Cards_Cards_h
#define Cards_Cards_h

#import <Foundation/Foundation.h>

@interface CDeck : NSObject

@property(strong,nonatomic) NSMutableArray *cards;

- (void)shuffle;

@end

#endif

