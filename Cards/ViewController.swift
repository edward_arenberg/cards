//
//  ViewController.swift
//  Cards
//
//  Created by Edward Arenberg on 9/23/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var swift : Bool {
        return false
    }

    private let cdeck = CDeck()
    private let sdeck = Deck()
    
    @IBOutlet var cardsCV : UICollectionView!
    
    @IBAction func shuffle() {
        if swift {
            sdeck.shuffle()
        } else {
            cdeck.shuffle();
        }
        cardsCV.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if swift {
            return sdeck.cards.count
        } else {
            return cdeck.cards.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CardCell", forIndexPath: indexPath) as UICollectionViewCell
        
        let label = cell.viewWithTag(1) as UILabel
        if swift {
            label.text = "\(sdeck.cards[indexPath.row])"
        } else {
            label.text = "\(cdeck.cards[indexPath.row])"
        }
        return cell
    }

}

