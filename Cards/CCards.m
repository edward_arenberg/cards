//
//  CCards.m
//  Cards
//
//  Created by Edward Arenberg on 9/23/14.
//  Copyright (c) 2014 EPage, Inc. All rights reserved.
//

#import "CCards.h"

// Enum = definition of same data kind
typedef enum {
   ace=1,two,three,four,five,six,seven,eight,nine,ten,jack,queen,king
} Rank;

typedef enum {
    spade = 0, heart, diamond, club
} Suit;


// Struct = collection of a variety of data types
typedef struct {
    Rank rank;
    Suit suit;
} CCard;


// Class = object with variables/properties and functions/methods
@interface CardClass : NSObject

@property (assign,nonatomic) CCard card;

@end

@implementation CardClass

- (instancetype)initWithCard:(CCard)card {
    if (self = [super init]) {
        _card = card;
    }
    return self;
}

// Methods to convert enum to string

- (NSString *)rankToString {
    NSString *s = @"U";
    switch (self.card.rank) {
        case ace:
            s = @"A";
            break;
        case two:
        case three:
        case four:
        case five:
        case six:
        case seven:
        case eight:
        case nine:
            s = [NSString stringWithFormat:@"%d",self.card.rank];
            break;
        case ten:
            s = @"T";
            break;
        case jack:
            s = @"J";
            break;
        case queen:
            s = @"Q";
            break;
        case king:
            s = @"K";
            break;
        default:
            break;
    }
    return s;
}

- (NSString *)suitToString {
    NSString *s = @"U";
    switch (self.card.suit) {
        case spade:
            s = @"♤";
            break;
        case heart:
            s = @"♡";
            break;
        case diamond:
            s = @"♢";
            break;
        case club:
            s = @"♧";
            break;
        default:
            break;
    }
    return s;
}

- (NSString *)description {
    NSString *s = [NSString stringWithFormat:@"%@%@",[self rankToString],[self suitToString]];
    return s;
}

@end


// Deck class to hold the cards
@implementation CDeck

- (instancetype) init {
    if (self = [super init]) {
        _cards = [self genDeck];
    }
    return self;
}

// Generate a deck of cards
- (NSMutableArray *) genDeck {
    NSMutableArray *cards = [NSMutableArray new];
    for (int i=0; i<4; i++) {
        for (int j=0; j<13; j++) {
            CCard card;
            card.rank = j+1;
            card.suit = i;
            CardClass *newCard = [[CardClass alloc] initWithCard:card];
            [cards addObject:newCard];
        }
    }
    return cards;
}

// Simple shuffle function to swap every card position
- (void)shuffle {
    NSUInteger numCards = [self.cards count];
    for (int i=0; i<numCards; i++) {
        int j = arc4random() % numCards;
        CardClass *swapCard = self.cards[i];
        [self.cards replaceObjectAtIndex:i withObject:self.cards[j]];
        [self.cards replaceObjectAtIndex:j withObject:swapCard];
    }
}


@end